# Web STL Viewer

This project creates a webpage to search for and preview STL files (and zip files containing stls) from a local directory.

This is my first full python app so there is a good chance none of this will work on any machine but mine.

[TOC]

## Requirements

1. python3 (I used python 3.8.10 for some reason)
2. a directory with stl files

## Installation

Create a `.env` file from `.env.example` and change the values as you see fit.

Change `app.py` line 12 to be the directory that holds the stl files

### Linux

After python is installed, I would set up a virtual environment for this first with:

`python3 -m venv venv`

and get into your virtual environment with:

`source venv/bin/activate`

Then to install the requirements:

`pip install`

Then run the web service with:

`flask run --host 0.0.0.0`

## Usage

After you have the app running go to the web address listed in the console or try `localhost:5000` if you are running it locally. You can also see /wordcloud to see a word cloud from all your stl files.
