import glob
from zipfile import ZipFile
from re import compile, IGNORECASE
from os import path, remove
from shutil import copyfile, move, rmtree
from flask import Flask, request, render_template, jsonify, send_from_directory
import operator

app = Flask(__name__)

def file_search(searchStr, showSupported, showBases):
	startDir = 'CHANGE_ME' # should absolute file path

	pattern = compile(searchStr+".*((\.zip)|(\.stl))", IGNORECASE)

	print('searching for: ' + searchStr)

	outfiles = []

	for filename in glob.iglob(startDir + '**/*.*', recursive=True):
		if '.zip' in filename:
			zipfile = filename
			archive = ZipFile(zipfile, 'r')
			files = archive.namelist()
			for file in files:
				if pattern.search(file):
					if showSupported == False and 'support' in file.lower() and not 'unsupported' in file.lower():
						continue
					if showBases == False and 'base' in file.lower():
						continue
					justName = ''
					if ('/' in file and not file.endswith('/')):
						justName = file.split('/')[-1:][0]
					else:
						justName = file
					outfiles.append(SearchResult(zipfile, file, justName, archive.getinfo(file).file_size))
		else:
			if pattern.search(filename):
				if showSupported == False and 'support' in file.lower() and not 'unsupported' in file.lower():
					continue
				if showBases == False and 'base' in filename.lower():
					continue
				justName = path.basename(filename)
				outfiles.append(SearchResult('', filename, justName, path.getsize(filename)))

	return outfiles

@app.route("/")
def index():
	return render_template("index.html")

@app.route('/stlfiles/<path:filename>')
def custom_static(filename):
    return send_from_directory('stlfiles/', filename)

@app.route("/search", methods=['GET'])
def response():
	searchStr = request.args.get("s")
	showSupported = request.args.get("suppt")
	if showSupported is not None: showSupported = True
	else: showSupported = False
	showBases = request.args.get("bases")
	if showBases is not None: showBases = True
	else: showBases = False

	results = []

	if len(searchStr) >= 3:
		for searchresult in file_search(searchStr=searchStr, showSupported=showSupported, showBases=showBases):
			#print(searchresult)
			results.append(searchresult)

	return render_template("index.html", searchStr=searchStr, results=results, showSupported=showSupported, showBases=showBases)

class SearchResult:	
	def __str__(self) -> str:
		return self.zipfile + " " + self.stlfile + self.justName

	def __init__(self, zipfile, stlfile, justName, filesize) -> None:
		self.stlfile = stlfile
		self.zipfile = zipfile
		self.justName = justName
		self.filesize = human_readable_size(filesize)

@app.route("/loadstl", methods=['POST'])
def loadStl():
	content = request.json

	stlcopypath = 'stlfiles'

	return_data = {} # return a status at least, if successful return url to stl
	
	# validate json
	#print(content)
	if not('zipfile' in content and 'stlfile'):
		return_data = jsonify({'status': 'bad json sent'}), 400

	copy_success = False
	justName = 'empty.error'
	if path.exists(content['stlfile']):
		justName = path.basename(content['stlfile'])
		copyfile(content['stlfile'], 'stlfiles/' + justName)
		#print('file copied')
		copy_success = True
		
	elif path.exists(content['zipfile']):
		#zipfilename = path.basename(content['zipfile'])
		with ZipFile(content['zipfile'], 'r') as zipObj:
			zipcontents = zipObj.namelist()
			if content['stlfile'] in zipcontents:
				if ('/' in content['stlfile'] and not content['stlfile'].endswith('/')):
					justName = content['stlfile'].split('/')[-1:][0]
				else:
					justName = content['stlfile']
				zipObj.extract(content['stlfile'], stlcopypath)
				copy_success = True
	else:
		return_data = jsonify({'status': 'error: files not found'}), 404

	if copy_success == True:
		flaten_folder()
		return_data = jsonify({'status': 'success', 'url': stlcopypath + '/' + justName}), 200
	
	return return_data

def flaten_folder():
	stlcopypath = 'stlfiles'

	for filename in glob.iglob(stlcopypath + '/**/*.*', recursive=True):
		#print(filename)
		if ".stl" in filename and not path.dirname(filename) == stlcopypath:
			if path.exists(stlcopypath + '/' + path.basename(filename)):
				remove(stlcopypath + '/' + path.basename(filename))
			move(filename, stlcopypath)

	for dir in glob.iglob(stlcopypath + '/*'):
		if path.isdir(dir):
			rmtree(dir)

@app.route("/wordcloud")
def wordcloud():
	results = file_search(searchStr='', showSupported=False, showBases=False)

	words = {}

	for result in map(lambda sr: sr.stlfile, results):
		re = result.lower()
		folders = re.split('/')
		re = folders[-1]
		folders = folders[:-1]

		re = re.replace('#', ' ').replace('-', ' ').replace('/', ' ').replace('(', ' ').replace(')', ' ').replace('_', ' ').replace('.', ' ').replace('[', ' ').replace(']', ' ')

		newWords = ''.join([i for i in re if not i.isdigit()]).split()
		newWords.extend(folders)
		for word in newWords:
			if not word.isnumeric():
				if word not in words:
					words[word] = 1
				else:
					words[word] += 1

	#print("Number of words before clean up: " + str(len(words)))

	delWords = []
	for word1 in words:
		for word2 in words:
			if len(word1) > 2 and word1 != word2 and (word1[-1] == 's' or word1[-2:] == "'s") and (word1[0:-1] == word2 or word1[0:-2] == word2):
				delWords.append(word1)
				words[word2] += words[word1]
			elif len(word1) <= 3:
				delWords.append(word1)

	for delWord in delWords:
		if delWord in words:
			words.pop(delWord)

	newWords = dict()

	maxCount = words['huge']
	#print(maxCount)

	for key, value in words.items():
		if value > 3 and value < maxCount:
			newWords[key] = value

	words = newWords

	words = dict(sorted(words.items(), key=operator.itemgetter(1),reverse=True))
	#print('Number of words after clean up: ' + str(len(words)))

	outList = []

	for key, value in words.items():
		outList.append({'x': key, 'value': value})
	
	return render_template('wordcloud.html', data=outList)

def human_readable_size(size, decimal_places=2):
		for unit in ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB']:
			if size < 1024.0 or unit == 'PiB':
				break
			size /= 1024.0
		return f"{size:.{decimal_places}f} {unit}"

@app.route("/wordcloudlist")
def wordcloudlist():
	results = file_search(searchStr='', showSupported=False, showBases=False)

	words = {}

	for result in map(lambda sr: sr.stlfile, results):
		re = result.lower()
		folders = re.split('/')
		re = folders[-1]
		folders = folders[:-1]

		re = re.replace('#', ' ').replace('-', ' ').replace('/', ' ').replace('(', ' ').replace(')', ' ').replace('_', ' ').replace('.', ' ').replace('[', ' ').replace(']', ' ')

		newWords = ''.join([i for i in re if not i.isdigit()]).split()
		newWords.extend(folders)
		for word in newWords:
			if not word.isnumeric():
				if word not in words:
					words[word] = 1
				else:
					words[word] += 1

	words = dict(sorted(words.items(), key=operator.itemgetter(1),reverse=True))

	return render_template('wordcloudlist.html', data=words)