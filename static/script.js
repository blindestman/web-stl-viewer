function load_prog(load_status, load_session) {
    var loaded=0;
    var total=0;
    
    //go over all models that are/were loaded
    Object.keys(load_status).forEach(function(model_id)
    {
        if (load_status[model_id].load_session==load_session) //need to make sure we're on the last loading session (not counting previous loaded models)
        {
            loaded+=load_status[model_id].loaded;
            total+=load_status[model_id].total;
        }
    });

    //set total progress bar
    var bar = document.querySelector(".progress-bar");
    var progress = Number((loaded/total) * 100).toFixed(0);
    bar.style.width = `${progress}%`;
    bar.setAttribute('aria-valuenow', progress);
    bar.textContent = progress + "%";
    console.log((loaded/total) * 100);
}

function all_loaded() {
    document.querySelector(".progress").style.display = "none";

    var lastRow = document.querySelector('.table-warning');
    lastRow.classList.remove('table-warning');
    lastRow.classList.add('table-success');
}

var stl_viewer;

function loadstl(event, zipfile, stlfile) {
    event.parentNode.parentNode.classList.add('table-warning');

    axios.post('/loadstl', {
            "zipfile": zipfile,
            "stlfile": stlfile
        })
        .then(function (resp) {
            console.log(resp.data);

            document.querySelectorAll(".stlviewer").forEach((element) => {
                element.style.display = "block";
            });
            removeAllChildNodes(document.querySelector('#stl_cont'));
            stl_viewer=new StlViewer(document.getElementById("stl_cont"), 
            {
                load_three_files: "",
                loading_progress_callback: load_prog,
                all_loaded_callback: all_loaded,
                models:[{
                    filename: "../../" + resp.data.url
                }]
            }
            );
        })
        .catch(function (error) {
            if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);

            var lastRow = document.querySelector('.table-warning');
            lastRow.classList.remove('table-warning');
            lastRow.classList.add('table-danger');
        });
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}